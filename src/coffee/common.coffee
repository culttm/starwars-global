# Utils
if !Object.keys
  Object.keys = (obj) ->
    keys = []

    for i in obj
      if obj.hasOwnProperty(i)
        keys.push(i)
    return keys


###
  Carousels initialization
  Setup custom settings
  owl.carousels
###

class Carousel
  constructor: (el) ->
    this.element = el
    this.defaults = {
      items: 1
      loop: true
      nav: false
      mouseDrag: false
      autoHeight: false
      navContainer: null
      responsive: {}
    }  

  init: ->
    def = @defaults
    buildCarousel this.element, def
    eventsControls this.element, def
    fireShowingControls this.element, def
    resizeWindow this.element

  jump: (to) ->
    jumpToCarousel this.element, to

  slideTo: (opt...) ->
    slideToCarousel opt...

  prepareResponseOption = (opts) ->

    for key, value of opts
      opts[key] = { 
        items: value
      }

    return opts

  updateOptions = (el, def) ->

    pluginOptions = $(el).data('plugin-options')
    responsiveOptions = $(el).data('responsive')
    plOpt = Object.keys(pluginOptions)

    if pluginOptions? and plOpt.length != 0
      $.extend def, pluginOptions

    if responsiveOptions? and plOpt.length != 0
      opts = prepareResponseOption responsiveOptions
      $.extend true, def.responsive, opts
    
    return def;

  jumpCarousel = (opt, to, speed) ->
    if typeof $.fn.owlCarousel != 'undefined'
      speed = speed or 1
      data = $(opt).owlCarousel()
      data.trigger 'to.owl.carousel', [to, speed] 

  buildCarousel = (el, def) ->
    if typeof $.fn.owlCarousel != 'undefined'
      o = updateOptions el, def
      owl = $(el).owlCarousel o

  eventsControls = (el) ->
    if typeof $.fn.owlCarousel != 'undefined'
      owl = $(el).owlCarousel()
      ctrl = $(el).parent()
        .find '[data-direction]'
      
      ctrl.on 'click', ->
        slideToCarousel owl, $(@).data().direction

  slideToCarousel = (owl, direction) ->
    owl.trigger direction+'.owl.carousel'

  jumpToCarousel = (owl, number) ->
    owl.trigger 'to.owl.carousel', number
  

  fireShowingControls = (el) ->
    item  = $(el).find('.owl-item').length
    owl = el.owlCarousel().data('owlCarousel')
    getVisItems = owl.settings.items

    if item < getVisItems
      $(el).parent()
        .find '[data-direction]'
        .hide()

  resizeWindow = (el) ->
    $(window).on 'resize', ->
      fireShowingControls el


# JS tabs

class JsTabs

  constructor: (args) ->
    @.container = args.container || $('.js-tabs')
    @.btn = args.container.find '[data-target]'
    @.label = args.container.find '.mobile-label'
    @.init()


  init: ->
    @.handler()
    @.changeLabelText()
    @.labelEvent()
    @.mobileListEvent()
    
  handler: ->
    self = @
    btn = self.btn

    $(btn).on 'click',  ->
      target = $(@).data 'target'
      t = $(@).text()
      i = $(@).data 'target'

      $(@)
        .addClass 'active'
        .siblings()
        .removeClass 'active'

      self.toggle(target)
      self.changeLabelText(t, i)


  toggle: (target) ->
    target = target || null
    $(@.container)
      .find '[data-panel="'+target+'"]'
      .addClass 'active'
      .siblings()
      .removeClass 'active'

  changeLabelText: (t, i) ->
    t = t || @.btn.first().text()
    i = i || @.btn.first().data 'target'
    label = $(@.container)
              .find  @.label
    mobList = $('[data-list="'+label.data('list-target')+'"]')
    label
      .text t

    mobList
      .find('[data-target="'+i+'"]')
      .addClass 'active'
      .siblings()
      .removeClass 'active'

  labelEvent: () ->
    $(@.label).on 'click', ->
      list = $(@).data 'list-target'
      $('[data-list="'+list+'"]')
        .addClass 'show'
        .css('padding-top', $(document).scrollTop())

  mobileListEvent: () ->
    ctx = $(@.label).data 'list-target'

    btn = $('[data-list="'+ctx+'"]').find('.tab-button')

    btn.on 'click', ->
      target = $(@).data 'target'
      tabcontainer = $(@).parents('.mobile-btn-list').data 'list'

      $('[data-list-target="'+tabcontainer+'"]')
          .parents('.js-tabs')
          .find '[data-target="'+target+'"]'
          .trigger 'click'
      $('.mobile-btn-list').removeClass 'show'

    $(document).on 'click', '.close', ->
      $('.mobile-btn-list').removeClass 'show'




$ ->
# Js Tabs
    
    $('.js-tabs').each ->
        tabs = new JsTabs({
            container: $(@)
          })


# Carousels init
    $('.carousel').each ->
        carousel = new Carousel($(@))
        carousel.init()



# Main slider init

    sliderOptions =
        sliderId: "main-slider"
        startSlide: 0
        effect: "8"
        effectRandom: false
        pauseTime: 5000
        transitionTime: 300
        slices: 20
        boxes: 8
        hoverPause: 1
        autoAdvance: true
        captionOpacity: 0.3
        captionEffect: "fade"
        thumbnailsWrapperId: "thumbs"
        m: false
        license: "mylicense"

    imageSlider=new mcImgSlider(sliderOptions)



# Footer toggle nav
    $(document).on 'click', '.footer .title', ->
      $(@).toggleClass 'active'




# Nav controls
    $(document).on 'click', '.toggle-nav', ->
      target = $(@).data 'target'
      $(@).toggleClass 'active'
      $('#'+target).toggleClass 'show' 


    fireMobileSize = ->
      if $(window).width() > 768
        $('body').removeClass 'xmd'
        $('.toggle-nav').removeClass 'active'
        $('.mobile-btn-list').removeClass 'show'
        $('.main-nav').removeClass 'show'
      else
        $('body').addClass 'xmd'


    fireMobileSize()    



    $(document).on 'click', '.have-child .root-item-link', (e) ->
      if $('body').hasClass 'xmd'
        e.preventDefault()
        $(@)
          .parent '.item'
          .toggleClass 'open'
          .find '.child'
          .toggleClass 'show'



    $(window).on 'resize', ->
      fireMobileSize()

<?php 
	$redirect =  $_GET["redirect"];
 ?> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"> 
<head>
	<meta charset="UTF-8">

	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@publisher_handle">
	<meta name="twitter:title" content="<?php echo $_GET["title"]; ?>">
	<meta name="twitter:description" content="<?php echo $_GET["description"]; ?>">
	<meta name="twitter:creator" content="@author_handle">
 	<meta name="twitter:image" content="<?php echo $_GET["image"]; ?>">

	<!-- Open Graph data -->
	<meta property="og:title" content="<?php echo $_GET["title"]; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://<?php echo $redirect; ?>" />
	<meta property="og:image" content="<?php echo $_GET["image"]; ?>" />
 	<meta property="og:description" content="<?php echo $_GET["description"]; ?>" /> 
 


</head>
<body>
	<script>
		var url = '<?php echo $redirect; ?>';

		if(typeof url !== 'undefined' && url !== ''){
			window.location.href = url;
		}else{
			history.back();
		}
	</script>
</body>
</html>
